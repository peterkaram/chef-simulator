﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SelectableUIObject : MonoBehaviour
{
    [SerializeField]
    private Image playerOneSelectionImage;

    [SerializeField]
    private Image playerTwoSelectionImage;

    public void SwitchSelection(Player player, bool isOn)
    {
        if(player == Player.One)
        {
            this.playerOneSelectionImage.gameObject.SetActive(isOn);
        }
        else if(player == Player.Two)
        {
            this.playerTwoSelectionImage.gameObject.SetActive(isOn);
        }
    }
}
