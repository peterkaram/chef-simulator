﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BarController : MonoBehaviour
{
    [SerializeField]
    private Image image;

    private float waitTime;

    private void Start()
    {
        this.enabled = false;
        this.StartCounting(20);
    }

    private void Update()
    {
        this.image.fillAmount -= 1.0f / this.waitTime * Time.deltaTime;
    }

    private void StartCounting(float waitTimeInSeconds)
    {
        if (this.enabled)
        {
            return;
        }

        this.waitTime = waitTimeInSeconds;
        this.enabled = true;
    }
}
