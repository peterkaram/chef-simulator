﻿using UnityEngine;
using UnityEngine.UI;

public enum VegetableType
{
    Lettuce,
    Onion,
    Potato,
}

public class Vegetable : MonoBehaviour 
{
    public VegetableType Type
    {
        get { return this.type; }
        set
        {
            this.type = value;
            this.GetComponentInChildren<Text>().text = value.ToString();
        }
    }

    [SerializeField]
    public SelectableUIObject VeggieSelector;

    private VegetableType type;
    
    public string Size { get; set; }
}
