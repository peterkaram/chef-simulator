﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CustomerUiController : MonoBehaviour
{
    [SerializeField]
    private Animator warningAnimator;

    [SerializeField]
    private BarController barController;

    [SerializeField]
    private float timeForWarning = 2;

    private float waitingTime;

    private void Start()
    {
        this.StartCoroutine(this.WaitAndShowWarningAnimation());
    }

    private IEnumerator WaitAndShowWarningAnimation()
    {
        yield return new WaitForSeconds(waitingTime < timeForWarning ? waitingTime : timeForWarning);
        this.warningAnimator.enabled = true;
    }
}