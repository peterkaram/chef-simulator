﻿using UnityEngine;

[System.Serializable]
public struct KeyConfig 
{
    public KeyCode Left;
    public KeyCode Right;
    public KeyCode Up;
    public KeyCode Down;
    public KeyCode Select;
}
