﻿using System.Collections.Generic;
using UnityEngine;

enum SelectionTarget
{
    LeftVegetables,
    OnChopTable,
    RightVegetables,
}

public enum Player
{
    One,
    Two,
}

public class MainController : MonoBehaviour
{
    [SerializeField]
    private Vegetable rightVegetableUi;

    [SerializeField]
    private Vegetable leftVegetableUi;

    [SerializeField]
    private KeyConfig playerOneControls;

    [SerializeField]
    private KeyConfig playerTwoControls;

    [SerializeField]
    private SelectableUIObject chopTable;

    private List<Vegetable> LeftSideVeggies = new List<Vegetable>();

    private List<Vegetable> rightSideVeggies = new List<Vegetable>();

    private KeyValuePair<SelectionTarget, int> playerOneSelection;

    private KeyValuePair<SelectionTarget, int> playerTwoSelection;

    private List<Vegetable> PlayerOneOnHandVeggies;

    private List<Vegetable> PlayerTwoOnHandVeggies;

    private Dictionary<SelectionTarget, List<Vegetable>> selectionTargetToInventoryList;
    
    private void Start()
    {            
        this.CreateVeggie(this.LeftSideVeggies, VegetableType.Lettuce, this.leftVegetableUi);
        this.CreateVeggie(this.LeftSideVeggies, VegetableType.Onion, this.leftVegetableUi);
        this.CreateVeggie(this.LeftSideVeggies, VegetableType.Potato, this.leftVegetableUi);

        this.CreateVeggie(this.rightSideVeggies, VegetableType.Lettuce, this.rightVegetableUi);
        this.CreateVeggie(this.rightSideVeggies, VegetableType.Onion, this.rightVegetableUi);
        this.CreateVeggie(this.rightSideVeggies, VegetableType.Potato, this.rightVegetableUi);

        this.selectionTargetToInventoryList =
        new Dictionary<SelectionTarget, List<Vegetable>>
        {
            {SelectionTarget.LeftVegetables , this.LeftSideVeggies },
            {SelectionTarget.RightVegetables, this.rightSideVeggies }
        };

        this.playerOneSelection = new KeyValuePair<SelectionTarget, int>(SelectionTarget.LeftVegetables, 0);
        this.playerTwoSelection = new KeyValuePair<SelectionTarget, int>(SelectionTarget.RightVegetables, 0);
        this.SwitchVeggieSelector(Player.One, true, this.playerOneSelection);
        this.SwitchVeggieSelector(Player.Two, true, this.playerTwoSelection);
    }   

    private void CreateVeggie(List<Vegetable> veggiesList, VegetableType type, Vegetable veggieUi)
    {
        var veggie = Instantiate(veggieUi, veggieUi.transform.parent);
        veggie.Type = type;
        
        veggiesList.Add(veggie);
        veggie.gameObject.SetActive(true);
    }

    private void Update()
    {
        if (!Input.anyKey)
        {
            return;
        }

        this.HandleInput(Player.One);
        this.HandleInput(Player.Two);
    }

    private void HandleInput(Player player)
    {
        var currentIndex = new KeyValuePair<SelectionTarget, int>();
        var controls = player == Player.One ? this.playerOneControls : this.playerTwoControls;
        if(player == Player.One)
        {
            currentIndex = this.playerOneSelection;          
        }
        else
        {
            currentIndex = this.playerTwoSelection;
        }

        var selectedList = currentIndex.Key == SelectionTarget.OnChopTable ? null : this.selectionTargetToInventoryList[currentIndex.Key];
        var selectedIndexNumber = currentIndex.Value;
        int? newIndexNumber = null;
        if (Input.GetKeyDown(controls.Down))
        {
            if (currentIndex.Key == SelectionTarget.OnChopTable)
            {
                return;
            }

            if (selectedIndexNumber == selectedList.Count - 1)
            {
                return;
            }

            newIndexNumber = selectedIndexNumber + 1;
        }

        else if (Input.GetKeyDown(controls.Up))
        {
            if (currentIndex.Key == SelectionTarget.OnChopTable)
            {
                return;
            }

            if (selectedIndexNumber == 0)
            {
                return;
            }

            newIndexNumber = selectedIndexNumber - 1;
        }

        else if (Input.GetKeyDown(controls.Right))
        {
            if (currentIndex.Key == SelectionTarget.RightVegetables)
            {
                return;
            }
            if (currentIndex.Key == SelectionTarget.LeftVegetables)
            {
                this.ChangeSelection(player, new KeyValuePair<SelectionTarget, int>(SelectionTarget.OnChopTable, 0), currentIndex);
            }
            // On chop table
            else
            {
                this.ChangeSelection(player, new KeyValuePair<SelectionTarget, int>(SelectionTarget.RightVegetables, 0), currentIndex);
            }
        }

        else if (Input.GetKeyDown(controls.Left))
        {
            if (currentIndex.Key == SelectionTarget.LeftVegetables)
            {
                return;
            }

            if (currentIndex.Key == SelectionTarget.RightVegetables)
            {
                this.ChangeSelection(player, new KeyValuePair<SelectionTarget, int>(SelectionTarget.OnChopTable, 0), currentIndex);
            }
            // On chop table
            else
            {
                this.ChangeSelection(player, new KeyValuePair<SelectionTarget, int>(SelectionTarget.LeftVegetables, 0), currentIndex);
            }
        }

        if (newIndexNumber.HasValue)
        {
            this.ChangeSelection(player, new KeyValuePair<SelectionTarget, int>(currentIndex.Key, newIndexNumber.Value), currentIndex);
        }
    }

    private void ChangeSelection(Player player, KeyValuePair<SelectionTarget, int> newSelectionIndex, KeyValuePair<SelectionTarget, int> currentSelectedIndex)
    {
        if(currentSelectedIndex.Key == SelectionTarget.OnChopTable)
        {
            this.chopTable.SwitchSelection(player, false);
        }
        else 
        {
            this.SwitchVeggieSelector(player, false, currentSelectedIndex);
        }

        if (player == Player.One)
        {
            this.playerOneSelection = newSelectionIndex;
        }
        else
        {
            this.playerTwoSelection = newSelectionIndex;
        }

        if (newSelectionIndex.Key == SelectionTarget.OnChopTable)
        {
            this.chopTable.SwitchSelection(player, true);
        }
        else
        {
            this.SwitchVeggieSelector(player, true, newSelectionIndex);
        }    
    }

    private void SwitchVeggieSelector(Player player, bool select, KeyValuePair<SelectionTarget, int> selectionIndex)
    {
        var veggie = this.selectionTargetToInventoryList[selectionIndex.Key][selectionIndex.Value];
        veggie.VeggieSelector.SwitchSelection(player, select);
    }
}